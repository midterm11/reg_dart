import 'package:flutter/material.dart';

class Dept extends StatelessWidget {
  const Dept({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text('ตรวจสอบหนี้'),
          backgroundColor: Colors.grey),
      body: ListView(
        children: <Widget>[
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Color.fromARGB(255, 56, 53, 53),
                ),
              ),
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.wallet,
                      ),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Image.network(
                              'http://www.pccm.ac.th/tinymce/home/articles/2018103002/pay-list-2-2561-nat.jpg',
                              fit: BoxFit.fill,
                            ),
                          ),
                        );
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.punch_clock,
                        //color: Colors.indigo.shade800,
                      ),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('คุณไม่มียอดค้างชำระ'),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            width: double.infinity,
            height: 200,
            child: Image.network(
              'https://www.ubu.ac.th/web/files/00014n2021090214330623.jpg',
              fit: BoxFit.fill,
            ),
          ),
          Container(
            width: double.infinity,
            height: 200,
            child: Image.network(
              'https://www.pangpond.com/wp-content/uploads/2021/06/%E0%B8%A7%E0%B8%B4%E0%B8%98%E0%B8%B5%E0%B9%80%E0%B8%8A%E0%B9%87%E0%B8%84%E0%B8%A2%E0%B8%AD%E0%B8%94-%E0%B8%81%E0%B8%A2%E0%B8%A8-4.png',
              fit: BoxFit.fill,
            ),
          ),
          Container(
            width: double.infinity,
            height: 200,
            child: Image.network(
              'https://ranong.ssru.ac.th/useruploads/images/20220119/38abaf249191978accb3c981ab47bae1b3343d43.png',
              fit: BoxFit.fill,
            ),
          ),
        ],
      ),
    );
  }
}
