import 'package:flutter/material.dart';


class Register extends StatelessWidget {
  const Register({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text('ลงทะเบียนเรียน'),
          backgroundColor: Colors.grey),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 200,
                child: Image.network(
                  'https://reg.buu.ac.th/ENROLLGUIDE_files/enroll.gif',
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                width: double.infinity,
                height: 70,
                color: Colors.amber,
                alignment: Alignment.center,
                child: const Text(
                  "ลงทะเบียนเรียนที่นี่",
                  style: TextStyle(color: Colors.red, fontSize: 50),
                ),
              ),
              Divider(
                color: Colors.grey,
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'โปรดใส่รหัสวิชาที่ต้องการลงทะเบียน',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 20,
                      ),
                    ),
                    TextField(
                      cursorColor: Colors.white,
                      decoration: InputDecoration(hintText: 'Enter here'),
                    )
                  ],
                ),
              ),
              Container(
                height: 50,
              ),
              Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(
                    child: const Text('ลงทะเบียน'),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          title: Text('ลงทะเบียนสำเร็จ'),
                        ),
                      );
                    },
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
