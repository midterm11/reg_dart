import 'package:flutter/material.dart';

class Tarang extends StatelessWidget {
  const Tarang({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text('ตารางเรียน'),
          backgroundColor: Colors.grey),
      body: ListView(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 200,
            child: Image.network(
              'https://sites.google.com/a/nareerat.ac.th/learning/_/rsrc/1488692173309/srang-nisay-kar-tangci-reiyn-ni-hxng/20130114153416.jpg',
              fit: BoxFit.fill,
            ),
          ),
          Table(
            border: TableBorder.all(),
            children: [
              buildRow([
                'Day/Time',
                '8:00-9:00',
                '9:00-10:00',
                '10:00-11:00',
                '11:00-12:00',
                '12:00-13:00',
                '13:00-14:00',
                '14:00-15:00',
                '16:00-17:00'
              ]),
              buildRow([
                'จันทร์',
                '',
                '',
                '',
                '',
                '',
                '66142364',
                '66142364',
                '66142364'
              ]),
              buildRow([
                'อังคาร',
                '',
                '86524215',
                '86524215',
                '',
                '',
                '5512462',
                '5512462',
                ''
              ]),
              buildRow([
                'พุธ',
                '',
                '56465122',
                '56465122',
                '',
                '',
                '554412462',
                '445512462',
                ''
              ]),
              buildRow(
                  ['พฤหัสบดี', '48966958', '48966958', '', '', '', '', '', '']),
              buildRow([
                'ศุกร์',
                '',
                '48966958',
                '48966958',
                '',
                '',
                '',
                '48966958',
                '48966958'
              ]),
              buildRow(['เสาร์', '', '', '', '', '', '', '', '']),
              buildRow(['อาทิตย์', '', '', '', '', '', '', '', '']),
            ],
          ),
          Container(
            width: double.infinity,
            height: 200,
          )
        ],
      ),
    );
  }

  TableRow buildRow(List<String> cells) => TableRow(
        children: cells.map((cell) => Text(cell)).toList(),
      );
}
