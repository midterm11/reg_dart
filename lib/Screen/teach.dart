import 'package:flutter/material.dart';


import 'register.dart';

class teach extends StatelessWidget {
  const teach({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text('วิชาที่เปิดสอน'),
          backgroundColor: Colors.grey),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 300,
                child: Image.network(
                  'https://image.dek-d.com/27/0271/8232/132994657',
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                height: 20,
              ),
              Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(
                    child: const Text(
                      'คลิ๊กเพื่อเข้าสู่หน้าจอลงทะเบียน',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return Register();
                      }));
                    },
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
