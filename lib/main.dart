import 'package:flutter/material.dart';
import 'dart:ui' ;
import 'Screen/Study.dart';
import 'Screen/calendar.dart';
import 'Screen/dept.dart';
import 'Screen/login.dart';
import 'Screen/tarang.dart';
import 'Screen/teach.dart';


void main() {
  runApp(Reg());
}

class MyApptheme {}

class Reg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Burapha University"),
          backgroundColor: Colors.grey,
          actions: <Widget>[
            IconButton(
              onPressed: () {
                print('Contact is starred');
              },
              icon: Icon(Icons.star_border),
              color: Colors.black,
            )
          ],
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              UserAccountsDrawerHeader(
                accountName: Text("62160242 : กีรติ ว่องไว "),
                accountEmail: Text("62160242@go.buu.ac.th"),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/288954397_3184073838535085_5237621127277538311_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeHx1a71O6N-BZXuON__VtfdzrOoJpUWgvrOs6gmlRaC-gRtRVfJgYE4YDQbnHEoExFYQHaf4kBca5ZA_KToWW40&_nc_ohc=G7McY9_X3EQAX8Xfz1o&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfD34fChJiht-jLz_OGjk0QsnHEzz46Rnet25D9LxcM_vQ&oe=63DBB9F3'),
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                      "https://p4.wallpaperbetter.com/wallpaper/858/577/307/black-black-desktop-hd-wallpaper-preview.jpg",
                    ),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              ListTile(
                title: Text('รหัสประจำตัว : 62160242'),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Text('เลขที่บัตรประชาชน:	1100703111158'),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Text('ชื่อ:	นายกีรติ ว่องไว'),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Text('คณะ:	คณะวิทยาการสารสนเทศ '),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Text('สัญชาติ:	ไทย '),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Text('ศาสนา:	พุทธ '),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Text('	หมู่เลือด:	A '),
              ),
              Divider(
                color: Colors.grey,
              ),
              ListTile(
                title: Text('อ. ที่ปรึกษา:	อาจารย์วรวิทย์ วีระพันธุ์ '),
              ),
              Divider(
                color: Colors.grey,
              ),
            ],
          ),
        ),
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(top: 8, bottom: 8),
                  child: Theme(
                    data: ThemeData(
                      iconTheme: IconThemeData(
                        color: Color.fromARGB(255, 56, 53, 53),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        buildHomeButton(),
                        buildtarangButton(),
                        buildGradeButton(),
                        buildRegisterButton(),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 8, bottom: 8),
                  child: Theme(
                    data: ThemeData(
                      iconTheme: IconThemeData(
                        color: Color.fromARGB(255, 56, 53, 53),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        buildDeptButton(),
                        buildcalendarButton(),
                        buildteachButton(),
                        buildlogoutButton(),
                      ],
                    ),
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
                Container(
                  width: double.infinity,
                  height: 300,
                  child: Image.network(
                    'https://reg.buu.ac.th/document/grad652.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 150,
                  child: Image.network(
                    'https://reg.buu.ac.th/anno/Line.jpg',
                    fit: BoxFit.fill,
                  ),
                ),
                Divider(
                  color: Colors.grey,
                ),
                Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Icon(Icons.phone),
                      ),
                      Text("095-512-5482")
                    ],
                  ),
                ),
                Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.facebook,
                        ),
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return Tarang();
                          }));
                        },
                      ),
                      Text("มหาวิทยาลัยบูรพา Burapha University")
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHomeButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.home,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Home()),
            );
          },
        ),
        Text("Home"),
      ],
    );
  }

  Widget buildtarangButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.calendar_month,
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return Tarang();
            }));
          },
        ),
        Text("ตารางเรียน"),
      ],
    );
  }

  Widget buildGradeButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.article,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return Study();
            }));
          },
        ),
        Text("ผลการศึกษา"),
      ],
    );
  }

  Widget buildRegisterButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.brush,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return Reg();
            }));
          },
        ),
        Text("ลงทะเบียนเรียน"),
      ],
    );
  }

  Widget buildDeptButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.money,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return Dept();
            }));
          },
        ),
        Text("ตรวจสอบหนี้"),
      ],
    );
  }

  Widget buildcalendarButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.calendar_month,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return calendar();
            }));
          },
        ),
        Text("ปฎิทินการศึกษา"),
      ],
    );
  }

  Widget buildteachButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.book_sharp,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return teach();
            }));
          },
        ),
        Text("วิชาที่เปิดสอน"),
      ],
    );
  }

  Widget buildlogoutButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.logout,
            //color: Colors.indigo.shade800,
          ),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return Login();
            }));
          },
        ),
        Text("ออกจากระบบ"),
      ],
    );
  }
}
